
# Covid situation info
This API provide a summary information about Covid-19 cases and make queries
by countries.
- All countries info
- Specific country info

[API home page](http://52.28.1.68/)
## General Architecture of project
This project creates an Amazon Cloudfront distribution on front of an Amazon API Gateway HTTP API and an AWS Lambda function that fetch information from disease.sh .
  ```mermaid
graph LR;
    A[AWS CloudFront]-->B[API Gateway];
    B[API Gateway]-->C[AWS Lambda];
    C[AWS Lambda]-->|External API call| D[disease.sh];
```
When a user makes request to our API, he is targeting AWS CloudFront.
AWS CloudFront caching all queries using `CachePolicy` that defined in `template.yaml` . Cache is set to 10m, but you can change this value at any time. In that way,  we have 10m information refresh rate, and avoid duplication of external API calls.

## Usage and deployment
### Prerequisites
- Node.js 18.x ([Download link](https://nodejs.org/en/download))
- AWS CLI ([Instalation guide here](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html))
- AWS SAM CLI ([Instalation guide here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/install-sam-cli.html))
- Docker 4.21.1 ([Install docker engine](https://docs.docker.com/engine/install/))
### Instalation
After you fulfill Prerequisites, you need to install all needed npm packages.
To do this you need to run `npm i` from the root repository folder  

### Start local environment
We are using AWS SAM to start local version thet work in exact same way as our lambdas togather with API gateway. To launch API locally you need to run

    sam local start-api

You should get the following message, after which you can test api
Containers Initialization is done.

    Mounting getSpecificCountryInfo at http://127.0.0.1:3000/covid-19/country/{id} [GET]
    Mounting getAllCountriesInfo at http://127.0.0.1:3000/covid-19/countries [GET]

Default mounting port is 3000, but if you want to change it, you can use -d parameter to specify it

    sam local start-api -d YOUR_DESIRED_PORT

> Note: If you where using build command before and your project contains
> ".aws-sam" folder, your live changes to the project code will be not
> applied. To avoid problems like this please delete ".aws-sam" folder
> before the launch of the local environment.

### Deployment
So deploy API to your AWS account you need to configure your AWS CLI user, which will be used to create and modify resources. ([Configure the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)).
Alternativle you can use `aws configure` comand. It require your `access_key` and `secret_key`
After configuration complete you can use following commands to deploy API:

    sam  build
    sam  deploy
[Specification of sam deploy comand](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-deploy.html)

To delete deployes stack, run:

    sam delete

### Unit tests
Tests are defined in the `__tests__` folder in this project. Use `npm` to install the [Jest test framework](https://jestjs.io/) and run unit tests.

    npm run test

## API documentation
I am using [Swagger](https://swagger.io/) to describe API specification.
To check how to interrract with API or test its work please use [this link](http://52.28.1.68/swagger). (Warrning!!! This link isn`t protected via SSL)

Also you can launch local version of the Swagger (React project). For this you need:

 - Go to the `front-prototype` folder
 - run `npm i` command
 - run `npm run start`
After this local version of the swagger should be available from [http://localhost:3000/swagger](http://localhost:3000/swagger)

## Project structure

### File structure
#### Api part. "covid-information-api" folder
-  `src` - Code for the application's Lambda functions.
-  `src/handlers` - Contains handlers for api endpoints.
-  `src/helpers` - Contains global reusable functions and classes.
-  `__tests__` - Unit tests for the application code.
-  `template.yaml` - A template that defines the application's AWS resources.

#### Frontend (Swagger) part. "front-prototype" folder
It`s just general description
- `src` - Code for the application.
- `public` - Contain `covid-info.json`(Swagger template). 

### CORS protection
Currentry CORS setiings lay in 2 seperate files:

 1. `template.yaml` - `Globals` section. (To apply CORS settings to AWS API Gateway during the deployment process)
 2. `src/helpers/lambdaResponse.mjs - useCors()` function. (To include needed headers into response)
If you want to change CORS settings you need to do this for both files.
> Latter those settings should be moved to environment variables.

## How to add new endpoint
To create new endpoint, you need to follow those steps:
- Create new AWS lambda handler. `covid-information-api/src/handlers`, you can use one of existing hendlers as a template. [See more about AWS Lambda function handler in Node.js](https://docs.aws.amazon.com/lambda/latest/dg/nodejs-handler.html)
- Add new `AWS::Serverless::Function` to the `template.yaml` and connect it to the new handler. [See more about AWS::Serverless::Function](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-resource-function.html)
- Add new unit test to the `covid-information-api/__tests__/unit/handlers`. [See more about JEST](https://jestjs.io/docs/getting-started)
- Describe your new API in Swagger template `front-prototype/public/covid-info.json` [See more about Swagger paths](https://swagger.io/docs/specification/paths-and-operations/)
- Test and deploy your new Endpoint