import * as React from 'react';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { coronaInfoActions } from '_store';
import { Grid, Container } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CountryPreview, DrawerAppBar } from '_components';
import ReactMarkdown from "react-markdown";
import "github-markdown.css"

export { Home };
const defaultTheme = createTheme();

function Home() {
    const [content, setContent] = useState("");
    const dispatch = useDispatch();
    //const countries = useSelector(store => store.corona.countries);
    useEffect(() => {
        fetch("readme.md")
            .then((res) => res.text())
            .then((text) => setContent(text));
    }, []);

    return (
        <>
            <DrawerAppBar>
                <ReactMarkdown className='markdown-body' >
                    {content}
                </ReactMarkdown>
                
            </DrawerAppBar>
        </>
    );
}
