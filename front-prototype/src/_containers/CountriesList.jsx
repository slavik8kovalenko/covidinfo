import * as React from 'react';
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { coronaInfoActions } from '_store';
import { Grid, Container } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CountryPreview, DrawerAppBar } from '_components';
import ReactMarkdown from "react-markdown";

import CssBaseline from '@mui/material/CssBaseline';
import Pagination from '@mui/material/Pagination';
import CircularProgress from '@mui/material/CircularProgress';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MailIcon from '@mui/icons-material/Mail';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import SearchIcon from '@mui/icons-material/Search';
import TextField from '@mui/material/TextField';
import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";

export { CountriesList };
const defaultTheme = createTheme();

function CountriesList() {
    const dispatch = useDispatch();
    const countries = useSelector(store => store.corona.countries);
    const listLoading = useSelector(store => store.corona.listLoading);
    useEffect(() => {
        dispatch(coronaInfoActions.getAllCountries())
    }, []);

    return (
        <>
            <DrawerAppBar>
                {listLoading?
                <CircularProgress color="success" />
                :                
                <Grid container spacing={2}>
                    {countries.map((post) => (
                        <CountryPreview key={post.country} post={post} />
                    ))}
                </Grid>
                }
            </DrawerAppBar>
        </>
    );
}
