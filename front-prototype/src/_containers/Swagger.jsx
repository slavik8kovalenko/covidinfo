import * as React from 'react';
import { useEffect } from 'react';
import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";
import { DrawerAppBar } from '_components';

export { Swagger };

function Swagger() {
    useEffect(() => {

    }, []);

    return (
        <>
            <DrawerAppBar>
                <SwaggerUI url="covid-info.json" />
            </DrawerAppBar>
        </>
    );
}
