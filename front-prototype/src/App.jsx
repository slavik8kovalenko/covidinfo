import { Routes, Route, Navigate, useNavigate, useLocation } from 'react-router-dom';

import { history } from '_helpers';
import { Nav, PrivateRoute } from '_components';
import { Home, Swagger, CountriesList } from './_containers';

export { App };

function App() {
    // init custom history object to allow navigation from 
    // anywhere in the react app (inside or outside components)
    history.navigate = useNavigate();
    history.location = useLocation();

    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/swagger" element={<Swagger />} />
            <Route path="/countries" element={<CountriesList />} />
        </Routes>
    );
}
