import * as React from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';

function CountryPreview(props) {
    const { post } = props;

    return (
        <Grid item xs={12} md={6}>
            <CardActionArea component="a" href="#">
                <Card sx={{ display: 'flex' }}>
                    <CardContent sx={{ flex: 1 }}>
                        <Typography component="h2" variant="h5">
                            {post.country}
                        </Typography>
                        <Typography variant="subtitle1" color="text.secondary">
                            {new Date(post.updated).toLocaleString()}
                        </Typography>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Typography variant="subtitle1">
                                    Today cases: {post.todayCases}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle1">
                                    Today deaths: {post.todayDeaths}
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Typography variant="subtitle1">
                                    Total cases: {post.cases}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="subtitle1">
                                    Total deaths: {post.deaths}
                                </Typography>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardMedia
                        component="img"
                        sx={{ width: 160, display: { xs: 'none', sm: 'block' } }}
                        image={post.countryInfo.flag}
                        alt={post.country}
                    />
                </Card>
            </CardActionArea>
        </Grid>
    );
}

CountryPreview.propTypes = {
    post: PropTypes.shape({
        updated: PropTypes.number.isRequired,
        country: PropTypes.string.isRequired,
        cases: PropTypes.number.isRequired,
        todayCases: PropTypes.number.isRequired,
        deaths: PropTypes.number.isRequired,
        todayDeaths: PropTypes.number.isRequired,
        countryInfo: PropTypes.object.isRequired,
    }).isRequired,
};

export { CountryPreview };