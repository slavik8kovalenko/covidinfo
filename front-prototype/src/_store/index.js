import { configureStore } from '@reduxjs/toolkit';

import { coronaInfoReducer } from './cronaInfo.slice';

export * from './cronaInfo.slice';

export const store = configureStore({
    reducer: {
        corona: coronaInfoReducer
    },
});
