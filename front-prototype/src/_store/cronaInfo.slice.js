import { create } from '@mui/material/styles/createTransitions';
import { createAsyncThunk, createSlice, createAction } from '@reduxjs/toolkit';

import { fetchWrapper } from '_helpers';
import { version } from 'react';

// create slice

const name = 'coronaInfo';
const initialState = createInitialState();
const extraActions = createExtraActions();
const extraReducers = createExtraReducers();
const slice = createSlice({ name, initialState, extraReducers });

// exports

export const coronaInfoActions = { ...slice.actions, ...extraActions };
export const coronaInfoReducer = slice.reducer;

// implementation

function createInitialState() {
    return {
        countries: []
    }
}
function createExtraActions() {
    const baseUrl = `${process.env.REACT_APP_API_URL}`;

    return {
        getAllCountries: getAllCountries(),
    };
    function getAllCountries() {
        return createAsyncThunk(
            `${name}/getAllCountries`,
            async (daysAgo, { getState }) => {
                return await fetchWrapper.get(`${baseUrl}/covid-19/countries`)
            }
        );
    }
}

function createExtraReducers() {
    return (builder) => {
        getAllCountries();

        function getAllCountries() {
            var { pending, fulfilled, rejected } = extraActions.getAllCountries;
            builder
                .addCase(pending, (state) => {
                    state.listLoading = true
                })
                .addCase(fulfilled, (state, action) => {
                    state.countries = action.payload                    
                    state.listLoading = false
                })
                .addCase(rejected, (state, action) => {                    
                    state.listLoading = true
                });
        }
    }
}
