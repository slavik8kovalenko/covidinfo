import { getAllCountriesHandler } from '../../../src/handlers/get-all-countries-info.mjs';
 
// This includes all tests for getAllCountriesHandler() 
describe('Test getAllCountriesHandler', () => { 
 
    it('Should return all countries info for today', async () => {
        const event = { 
            httpMethod: 'GET' 
        };
 
        // Invoke helloFromLambdaHandler() 
        const result = await getAllCountriesHandler(event);
 
        // Compare the result with the expected result 
        expect(result.statusCode).toEqual(200)
        let body = JSON.parse(result.body)
        expect(typeof body).toBe('object')
        body.map((item)=>{
            expect("updated" in item).toBe(true)
            expect("country" in item).toBe(true)
            expect("cases" in item).toBe(true)
            expect("todayCases" in item).toBe(true)
            expect("deaths" in item).toBe(true)
            expect("todayDeaths" in item).toBe(true)
            expect("recovered" in item).toBe(true)
            expect("todayRecovered" in item).toBe(true)
            expect("active" in item).toBe(true)
            expect("critical" in item).toBe(true)
            expect("casesPerOneMillion" in item).toBe(true)
            expect("deathsPerOneMillion" in item).toBe(true)
            expect("tests" in item).toBe(true)
            expect("testsPerOneMillion" in item).toBe(true)
            expect("population" in item).toBe(true)
            expect("continent" in item).toBe(true)
            expect("oneCasePerPeople" in item).toBe(true)
            expect("oneDeathPerPeople" in item).toBe(true)
            expect("oneTestPerPeople" in item).toBe(true)
            expect("activePerOneMillion" in item).toBe(true)
            expect("recoveredPerOneMillion" in item).toBe(true)
            expect("criticalPerOneMillion" in item).toBe(true)
            expect("countryInfo" in item).toBe(true)
        })
    }); 
    
    it('Should return daysAgo parametr error', async () => {
        const event = { 
            httpMethod: 'GET',
            queryStringParameters: { 
                daysAgo: '3' 
            } 
        };
 
        // Invoke helloFromLambdaHandler() 
        const result = await getAllCountriesHandler(event);
 
        // Compare the result with the expected result 

        expect(result.statusCode).toEqual(422)  // // Unprocessable entity        
        let body = JSON.parse(result.body)
        expect(body.message).toContain(`Invalid value for "daysAgo" use one of`)
    }); 
}); 
