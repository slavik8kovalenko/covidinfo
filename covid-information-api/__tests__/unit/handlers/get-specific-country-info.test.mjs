import { getSpecificCountryHandler } from '../../../src/handlers/get-specific-country-info.mjs';
 
// This includes all tests for getSpecificCountryHandler() 
describe('Test getSpecificCountryHandler', () => { 
 
    it('Should return Haiti info for today', async () => {
        const event = { 
            httpMethod: 'GET',
            pathParameters: {
                id: "Haiti"
            }
        };
        const result = await getSpecificCountryHandler(event);
 
        expect(result.statusCode).toEqual(200)
        let body = JSON.parse(result.body)
        expect(typeof body).toBe('object')
            expect("updated" in body).toBe(true)
            expect(body.country).toBe("Haiti")
            expect("cases" in body).toBe(true)
            expect("todayCases" in body).toBe(true)
            expect("deaths" in body).toBe(true)
            expect("todayDeaths" in body).toBe(true)
            expect("recovered" in body).toBe(true)
            expect("todayRecovered" in body).toBe(true)
            expect("active" in body).toBe(true)
            expect("critical" in body).toBe(true)
            expect("casesPerOneMillion" in body).toBe(true)
            expect("deathsPerOneMillion" in body).toBe(true)
            expect("tests" in body).toBe(true)
            expect("testsPerOneMillion" in body).toBe(true)
            expect("population" in body).toBe(true)
            expect("continent" in body).toBe(true)
            expect("oneCasePerPeople" in body).toBe(true)
            expect("oneDeathPerPeople" in body).toBe(true)
            expect("oneTestPerPeople" in body).toBe(true)
            expect("activePerOneMillion" in body).toBe(true)
            expect("recoveredPerOneMillion" in body).toBe(true)
            expect("criticalPerOneMillion" in body).toBe(true)
            expect("countryInfo" in body).toBe(true)
    }); 
    
    it('Should return daysAgo parametr error', async () => {
        const event = { 
            httpMethod: 'GET',
            pathParameters: {
                id: "Haiti"
            },
            queryStringParameters: { 
                daysAgo: '3' 
            }
        };
        const result = await getSpecificCountryHandler(event);

        expect(result.statusCode).toEqual(422)  // // Unprocessable entity        
        let body = JSON.parse(result.body)
        expect(body.message).toContain(`Invalid value for "daysAgo" use one of`)
    }); 

    it('Should return 400 Cant process your request(not existed country used)', async () => {
        const event = { 
            httpMethod: 'GET',
            pathParameters: {
                id: "Something"
            }
        };
        const result = await getSpecificCountryHandler(event);

        expect(result.statusCode).toEqual(400)  // // Unprocessable entity        
        let body = JSON.parse(result.body)
        expect(body.message).toContain(`Cant process your request`)
    }); 
}); 
