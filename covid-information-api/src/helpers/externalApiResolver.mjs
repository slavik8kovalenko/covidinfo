import axios from "axios"

export function externalApiResolver(config) {
    let externalRequestHandler = new Promise((resolve, reject) => {
        axios.request(config)
            .then((externalApiResponse) => {
                const response = {
                    statusCode: 200,
                    body: JSON.stringify(
                        externalApiResponse.data
                    )
                };
                resolve(response)
            })
            .catch((error) => {
                const response = {
                    statusCode: 400,
                    body: JSON.stringify({
                        message: "Cant process your request"
                    })
                };
                resolve(response)
            });
    })
    return externalRequestHandler
}
export function makeRequestConfiguration({baseConfig, useDaysAgo}) {
    const daysAgoOptions = ['0', '1', '2'] // available parameter values
    let diseaseRequestConfig = {
        method: 'get',
        maxBodyLength: Infinity,
        url: 'https://disease.sh/v3/covid-19/countries',
        headers: {},
        ...baseConfig,
    };
    if(typeof useDaysAgo === "string") {
        if(daysAgoOptions.includes(useDaysAgo) === true) {
            switch (useDaysAgo) {
                case '1':
                    diseaseRequestConfig.params = {
                        yesterday: true
                    }
                break
                case '2':
                    diseaseRequestConfig.params = {
                        twoDaysAgo: true
                    }
                break
            }            
        } else {
            return {
                validationError: {
                    statusCode: 422, // Unprocessable entity
                    body: JSON.stringify({
                        message: `Invalid value for "daysAgo" use one of ${JSON.stringify(daysAgoOptions)}`
                    })
                }
            }
        } 
    }
    return diseaseRequestConfig
}