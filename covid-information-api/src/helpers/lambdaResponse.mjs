export class LambdaResponce {
    response = {
        headers: {},
        statusCode: 200,
        body: ""
    }

    constructor({statusCode = false, body = false} = {statusCode: false, body: false}) {
        if(body) { this.response.body = body }
        if(statusCode) { this.response.statusCode = statusCode }
        return this
    }

    setBasicResponse({statusCode = false, body = false}) {
        if(body) { this.response.body = body }
        if(statusCode) { this.response.statusCode = statusCode }
        return this
    }

    useCors() {
        this.response.headers = {
                ...this.response.headers,
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*", // Allow from anywhere 
                "Access-Control-Allow-Methods": "GET" // Allow only GET request 
            }
        return this
    }
}