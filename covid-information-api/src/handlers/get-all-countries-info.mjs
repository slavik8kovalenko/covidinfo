import { externalApiResolver, makeRequestConfiguration } from "../helpers/externalApiResolver.mjs"
import { LambdaResponce } from '../helpers/lambdaResponse.mjs'

export const getAllCountriesHandler = async (event) => {
    let makeLambdaResponse = new LambdaResponce().useCors()
    if (event.httpMethod !== 'GET') {
        throw new Error(`getAllItems only accept GET method, you tried: ${event.httpMethod}`);
    }
    let toMakeConfig = {
        baseConfig: {
            url: 'https://disease.sh/v3/covid-19/countries'
        }
    }
    if (event.queryStringParameters && "daysAgo" in event.queryStringParameters) {  // daysAgo parameter handeled as a string
        toMakeConfig.useDaysAgo = event.queryStringParameters["daysAgo"]
    }
    let requestConfig = makeRequestConfiguration(toMakeConfig)
    if (requestConfig.validationError) {
        return makeLambdaResponse.setBasicResponse(requestConfig.validationError).response
    }
    return makeLambdaResponse.setBasicResponse( await externalApiResolver(requestConfig) ).response
}
